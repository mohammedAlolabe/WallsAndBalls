using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class HouseRoomsSO : ScriptableObject
{   
    public List<RoomTypeSO> roomTypes; 
}
