using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wrench : MonoBehaviour
{
    public Image wrenchEmpty;
    public Sprite wrenchFull;

    private void Start() {
        GlobalVariablesManager.SetWrench(this); 
    }
    public void SetWrenchOn()
    {
        wrenchEmpty.sprite = wrenchFull;
    }
}