using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    [SerializeField] private GameObject resumeButton;
    [SerializeField] private Button mainMenuButton;
    [SerializeField] private Button quitButton1;
    public GameObject pauseMenuUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !GlobalVariablesManager.GameEnded())
        {
            if (GameIsPaused)
            {
                Resume();
                
            }
            else
            {
                Pause();
            }
        }
      
    }

    private void Awake()
    {
        mainMenuButton.onClick.AddListener(()=>{
            // Clean up references or perform any necessary cleanup tasks
            GlobalVariablesManager.ClearPlayerAndDroneReferences();

            // Unload the current scene
            SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
            SceneManager.LoadScene(0);
        });
        
        quitButton1.onClick.AddListener(()=>{
            Application.Quit();
        });
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    void Pause()
    {
        pauseMenuUI.SetActive(true);
        EventSystem.current.SetSelectedGameObject(resumeButton);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        GameIsPaused = false;
        SceneManager.LoadScene(" ");
    }
  
}
