using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StarterAssets.ThirdPersonController.Scripts
{
    public class WallChecked : MonoBehaviour
    {
        public Color wallColor;
        public Collider wallCollider;
        private Skillcd _skillCooldown;
        public bool isShiftKeyPressed = false;
        private AudioSource _audioSource; // Reference to the AudioSource component
        public AudioClip skillOnCooldownClip; // Assign the audio clip in the Inspector
        private PlayerTrail _pt;
        private float originalTimeScale;


        private void Start()
        {
            // Find the Skillcd component under the Canvas
            Transform canvasTransform = GameObject.Find("Canvas/Bar").transform;
            if (canvasTransform != null)
            {
                Transform skillcdTransform = canvasTransform.Find("Skill");
                if (skillcdTransform != null)
                {
                    _skillCooldown = skillcdTransform.GetComponent<Skillcd>();
                    if (_skillCooldown == null)
                    {
                        Debug.LogError("Skillcd component not found under the Canvas!");
                    }
                }
                else
                {
                    Debug.LogError("Skillcd GameObject not found under the Canvas!");
                }
            }
            else
            {
                Debug.LogError("Canvas GameObject not found in the scene!");
            }
        }

        private void Update()
        {
            // Check if the Shift key is pressed
            isShiftKeyPressed = Input.GetKey(KeyCode.LeftShift);
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
             
                _pt = other.GetComponent<PlayerTrail>();
                _skillCooldown.isInsideCollider = true;
                global::ThirdPersonController tpc = other.gameObject.GetComponent<global::ThirdPersonController>();
                GameObject coloredBall = tpc.transform
                    .Find(
                        "Skeleton/Hips/Spine/Chest/UpperChest/Right_Shoulder/Right_UpperArm/Right_LowerArm/Right_Hand/RightBall")
                    .gameObject;
                GameObject coloredBall2 = tpc.transform
                    .Find(
                        "Skeleton/Hips/Spine/Chest/UpperChest/Left_Shoulder/Left_UpperArm/Left_LowerArm/Left_Hand/LeftBall")
                    .gameObject;
                _audioSource = tpc.transform.Find("Dash sfx").GetComponent<AudioSource>();
                Renderer ballRenderer = coloredBall.GetComponent<Renderer>();
                Renderer ballRenderer2 = coloredBall2.GetComponent<Renderer>();

                //Color wallColor = GetComponent<Renderer>().material.color; // Assuming the wall has a Renderer component with a material

                if (ballRenderer.sharedMaterial != null && coloredBall.activeSelf)
                {
                    Color ballColor = ballRenderer.material.color;

                    if (ColorsMatch(ballColor, wallColor))
                    {
                        wallCollider.isTrigger = true;
                        Debug.Log("Colors match!");
                        // Additional actions when colors match
                        tpc.canSprint = true;
                        
                    }
                    else
                    {
                        // Colors do not match, try to use the skill
                        if (isShiftKeyPressed && _skillCooldown.CanUseSkill())
                        {
                            wallCollider.isTrigger = true;

                            _skillCooldown.UseSkill();
                            tpc.canSprint = true;
                            
                            Debug.Log("Using the skill!");
                            // Play sound when skill is on cooldown
                            if (_audioSource != null && skillOnCooldownClip != null)
                            {
                                _audioSource.PlayOneShot(skillOnCooldownClip);
                            }
                            if (!_pt.isTrailActive )
                            {
                                _pt.StartCoroutine(_pt.ActiveTrail(_pt.activeTime));
                                StartDashAbility();
                            }
                        }
                        else
                        {
                            Debug.Log("Cannot use the skill - still on cooldown!");
                        }
                    }
                }
                else if (ballRenderer2.sharedMaterial != null && coloredBall2.activeSelf)
                {
                    Color ballColor2 = ballRenderer2.material.color;

                    if (ColorsMatch(ballColor2, wallColor))
                    {
                        wallCollider.isTrigger = true;
                        Debug.Log("Colors match!");
                        // Additional actions when colors match
                        tpc.canSprint = true;
                    }
                    else
                    {
                        // Colors do not match, try to use the skill
                        if (isShiftKeyPressed && _skillCooldown.CanUseSkill())
                        {
                            wallCollider.isTrigger = true;

                            _skillCooldown.UseSkill();
                            tpc.canSprint = true;
                            Debug.Log("Using the skill!");
                            if (_audioSource != null && skillOnCooldownClip != null)
                            {
                                _audioSource.PlayOneShot(skillOnCooldownClip);
                            }
                            if (!_pt.isTrailActive )
                            {
                                _pt.StartCoroutine(_pt.ActiveTrail(_pt.activeTime));
                                StartDashAbility();
                            }
                        }
                        else
                        {
                            Debug.Log("Cannot use the skill - still on cooldown!");
                        }
                    }
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                _skillCooldown.isInsideCollider = false;
                global::ThirdPersonController tpc = other.gameObject.GetComponent<global::ThirdPersonController>();
                tpc.canSprint = false;
                wallCollider.isTrigger = false;
                //EndDashAbility();
            }
        }

        private bool ColorsMatch(Color color1, Color color2)
        {
            return Mathf.Approximately(color1.r, color2.r)
                   && Mathf.Approximately(color1.g, color2.g)
                   && Mathf.Approximately(color1.b, color2.b)
                   && Mathf.Approximately(color1.a, color2.a);
        }
        private void StartDashAbility()
        {
            // Store the current time scale before adjusting it
            originalTimeScale = Time.timeScale;

            // Start slow motion by adjusting the time scale
            Time.timeScale = 0.3f; // Adjust this value for the desired slow-motion intensity

            // Start the dash ability here
            StartCoroutine(DashAbilityCoroutine());
        }
        private void EndDashAbility()
        {
            // End the slow motion by restoring the original time scale
            Time.timeScale = originalTimeScale;

            // End the dash ability here
        }
        private IEnumerator DashAbilityCoroutine()
        {
            // Perform any setup before the dash

            // Dash logic
            // This might involve moving the player quickly in a certain direction

            yield return new WaitForSeconds(0.3f); // Wait for the dash to complete

            // Perform any cleanup after the dash

            EndDashAbility(); // Call the method to end the dash ability
        }



        
    }
}