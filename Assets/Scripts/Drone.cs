using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Drone : MonoBehaviour
{
    [SerializeField] 
    private int droneColor = 2;

    [SerializeField]
    private int idleSpeed = 4, chasingSpeed = 7, chasingDistance = 20, damageDistance = 10; 

    private int n, m;
    private List<KeyValuePair<int, int>> accessedCellsIndices;
    private bool[,] vis;
    private bool activated = false, firstGoal = true;

    private List<KeyValuePair<int, int>>[,] adj; 
    private Dictionary<KeyValuePair<int, int> , int> bfs; 

    private KeyValuePair<int, int>  droneIndices, goalIndices; 
    private Player chasedPlayer; 


    
    private void Update()
    {
        if (!activated) return;

        droneIndices = GetCellIndices(transform.position.x, transform.position.z);
    
        if(NearPlayer()) 
        {
            // Debug.Log("CHASING");
            Chase(); 
        }
        else Idle();
    }

    public void ActivateDrone()
    {
        n = GlobalVariablesManager.ReturnWidth();
        m = GlobalVariablesManager.ReturnHeight();

        vis = new bool[n, m];
        for(int i=0 ;i < n ; i ++) for(int j=0 ; j < m ; j ++) vis[i, j] = false; 

        adj = GlobalVariablesManager.GetAdjacList(droneColor);
        
        droneIndices = GetCellIndices(transform.position.x, transform.position.z);

        accessedCellsIndices = new List<KeyValuePair<int, int>>();         
        Dfs(droneIndices.Key, droneIndices.Value);

        bfs = new Dictionary<KeyValuePair<int, int> , int> ();
        /* 
                        bfs moves : 
                            012
                            345
                            678   
        */

        foreach(var point in accessedCellsIndices)
        {
            for(int i=0 ;i < n ; i ++) for(int j=0 ; j < m ; j ++) vis[i, j] = false; 
            int sorceI = point.Key, sorceJ = point.Value, sorceHash = GetPointHash(sorceI, sorceJ); 


            Queue<KeyValuePair<KeyValuePair<int, int>, int >> q = new Queue<KeyValuePair<KeyValuePair<int, int>, int >>(); 

            foreach(var son in adj[sorceI, sorceJ])
            {   
                int ii = son.Key, jj = son.Value; 

                int move = 0; 
                for(int di = +1 ; di >= -1 ; di--) for(int dj = -1 ; dj <= +1 ; dj ++)
                {
                    if((sorceI + di == ii) && (sorceJ + dj == jj))
                    {
                        vis[ii, jj] = true; 
                        q.Enqueue(Pair(Pair(ii, jj), move)); 
                        bfs.Add(Pair(sorceHash, GetPointHash(ii, jj)), move);
                    }
                    move ++; 
                }
            }

            while(q.Count != 0)
            {
                var node = q.Peek(); 
                int nodeI = node.Key.Key, nodeJ = node.Key.Value, mv = node.Value; 
                q.Dequeue(); 

                foreach(var son in adj[nodeI, nodeJ]) if(!vis[son.Key, son.Value])
                {
                    vis[son.Key, son.Value] = true; 
                    q.Enqueue(Pair(Pair(son.Key, son.Value), mv)); 
                    bfs.Add(Pair(sorceHash, GetPointHash(son.Key, son.Value)), mv); 
                }
            }
        }

        activated = true;
        goalIndices = droneIndices; 
    }

    private void Dfs(int i, int j)
    {
        vis[i, j] = true; 
        accessedCellsIndices.Add(new KeyValuePair<int, int>(i, j));
        foreach(var point in adj[i, j]) if(vis[point.Key, point.Value] == false)
            Dfs(point.Key, point.Value);
    }




    public bool NearPlayer()
    {
        const float BIG_VALUE = 10000000; 
        float mnDist = BIG_VALUE;

        foreach(Player player in GlobalVariablesManager.GetPlayers())
        {
            double x = player.transform.position.x, y = player.transform.position.z; 

            double dx = Math.Abs(x - transform.position.x), 
                    dy = Math.Abs(y - transform.position.z); 


            float dist = (float)Math.Sqrt(dx * dx + dy * dy); 

            var pIndices = GetCellIndices((float)x, (float)y); 
            
            var pKey = Pair(GetPointHash(droneIndices.Key, droneIndices.Value), 
                                        GetPointHash(pIndices.Key, pIndices.Value)); 
                        
            if(bfs.ContainsKey(pKey) && dist < mnDist && dist <= chasingDistance)
            {
                mnDist = dist; 
                chasedPlayer = player; 
                goalIndices = GetCellIndices((float)x, (float)y); 
            }
        }

        return mnDist != BIG_VALUE; 
    }

    private void HurtPlayer()
    {
        /*
            DO YOUR WORK HERE !!!

            chased player is saved in chasedPlayer variable.

        */
    }

    private void Chase()
    {
        MoveByType(bfs[Pair(GetPointHash(droneIndices.Key, droneIndices.Value), GetPointHash(goalIndices.Key, goalIndices.Value))], true);
        HurtPlayer(); 
    }

    private void Idle()
    {
        if(firstGoal || (droneIndices.Key == goalIndices.Key && droneIndices.Value == goalIndices.Value))
        {
            // set a new goal, the previous when was reached
            firstGoal = false;
            goalIndices = GetRandomAccessedCell(); 
            Idle();
            return; 
        }
        
        var bfsKey = (Pair(GetPointHash(droneIndices.Key, droneIndices.Value), GetPointHash(goalIndices.Key, goalIndices.Value))); 
        if(bfs.ContainsKey(bfsKey))
        {
            MoveByType(bfs[bfsKey], false); 
        }
        else 
        {
            Debug.LogError("KEY NOT FOUND");
        }
    } 

    private void MoveByType(int move, bool isChasing)
    {
        Vector3 transition = Vector3.zero; 


        float speed = (isChasing ? chasingSpeed : idleSpeed); 
        float dist = Time.deltaTime * speed;
        

        switch (move)
        {
            case 1:
                transition.z = dist; 
                break;
            
            case 7: 
                transition.z = - dist; 
                break;
            
            case 5:
                transition.x = dist; 
                break;
            
            case 3: 
                transition.x = - dist; 
                break;
        }

        transform.position += transition; 
    }

    public int GetDamageDistance()
    {
        return damageDistance; 
    }











    // helper functions : 

    private KeyValuePair<int, int> GetCellIndices(float x, float y)
    {
        x -= GlobalVariablesManager.GetMinX(); 
        y -= GlobalVariablesManager.GetMinY(); 

        return new KeyValuePair<int, int>((int)(y / GlobalVariablesManager.GetCellLength()), (int)
            (x / GlobalVariablesManager.GetCellLength()));
    }


    private int GetPointHash(int i, int j)
    {
        return i * 1000 + j; 
    }

    private KeyValuePair<int, int> GetPoint(int hash)
    {
        int x = hash / 1000; 
        int y = hash - x * 1000; 

        return new KeyValuePair<int, int>(x, y); 
    }
    
    private KeyValuePair<int,int> Pair(int i, int j)
    {
        return new KeyValuePair<int, int>(i, j); 
    }

    private KeyValuePair<KeyValuePair<int, int>,int> Pair(KeyValuePair<int, int> i, int j)
    {
        return new KeyValuePair<KeyValuePair<int, int>,int>(i, j); 
    }
    private KeyValuePair<int, int> GetRandomAccessedCell()
    {
        int randomIndex = HouseGenerator.rnd.Next(0, accessedCellsIndices.Count); 
        return accessedCellsIndices[randomIndex]; 
    }
}