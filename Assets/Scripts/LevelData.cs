public class LevelData {
    
    public int n, m; 
    public Cell[, ] grid; 

    public LevelData(int np, int mp, Cell[, ] gridp)
    {
        n = np; 
        m = mp; 
        grid = gridp;
    }
    public string LevelDataToString()
    {
        string[] cellStrings = new string[n * m + 2];

        cellStrings[0] = n.ToString(); 
        cellStrings[1] = m.ToString(); 

        int ind = 2;  // first two indices [0, 1] for [n, m]
        for(int i=0 ; i < n ; i ++)
            for(int j=0 ; j < m ; j ++)
            {
                cellStrings[ind] = grid[i, j].StringifyCell(); 
                ind ++; 
            }
        
        return string.Join("*", cellStrings);
    }

    public LevelData(string s)
    {
        string[] data = s.Split('*'); 
        n = int.Parse(data[0]); 
        m = int.Parse(data[1]); 
        grid = new Cell[n, m]; 

        int ind = 2; 
        for(int i=0 ;i < n ; i ++)
            for(int j=0 ; j < m; j ++)
            {
                string[] cellData = data[ind].Split(',');
                int[] ar = new int[19]; 
                for(int k=0 ; k < 19 ; k++)
                    ar[k] = int.Parse(cellData[k]); 

                grid[i, j] = new Cell(ar); 

                ind ++; 
            }
    }
}
