using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public static class GlobalVariablesManager
{
    private static List<Player> players = new List<Player>();
    private static List<Drone> drones = new List<Drone>();
    private static List<Material> ballsMaterials = new List<Material>(); 
    private static int n, m;
    private static float mnX, mnY;
    private static float cellLength, diagonalLength;
    private static Menus winLosePause; 
    private static HouseRoomsSO houseRoomsSO; 
    
    private static int NearBall;
    private static bool NearBallExist = false; 

    private static Cell[,] houseGrid;

    private static  bool tool1 = false, tool2 = false; 
    
    private static bool gameEnded = false; 

    private static TakeDamage takeDamage; 

    private static Content content; 

    private static ScrewDriver screwDriver; 
    private static Wrench wrench; 

    private static string currentLevelString; 


    public static void SetHouseRoomSO(HouseRoomsSO hrso)
    {
        houseRoomsSO = hrso; 
    }
    public static HouseRoomsSO GetHouseRooms()
    {
        return houseRoomsSO; 
    }
    public static string GetCurrentLevelString()
    {
        return currentLevelString; 
    }

    public static void SetCurrentLevelString(string s)
    {
        currentLevelString = s; 
    }

    public static void SetScewDriver(ScrewDriver scr)
    {
        screwDriver = scr;
    }

    public static void SetWrench(Wrench wr)
    {
        wrench = wr; 
    }
    public static void SetContent(Content cnt)
    {
        content = cnt; 
    }

    public static Content GetContent()
    {
        return content; 
    }


    public static void SetTakeDamage(TakeDamage tk)
    {
        takeDamage = tk; 
    }
    public static TakeDamage GetTakeDamage()
    {
        return takeDamage; 
    }
    public static void SetWinLosePause(Menus menu)
    {
        winLosePause = menu; 
    }

    public static Menus GetWinLosePause()
    {
        return winLosePause; 
    }

    public static void AddMaterial(Material material)
    {
        ballsMaterials.Add(material);
    }

    public static List<Material> GetBallsMaterials()
    {
        return ballsMaterials; 
    }

    public static void SetTool(int i)
    {
        if(i == 2) 
        {
            screwDriver.SetScrewDriverOn(); 
            tool1 = true; 
        }

        if(i == 1) 
        {
            wrench.SetWrenchOn(); 
            tool2 = true;
        }
    }   

    public static bool HaveAllTools()
    {
        return tool1 & tool2; 
    }


    public static void EndGame()
    {
        gameEnded = true; 
    }

    public static bool GameEnded()
    {
        return gameEnded; 
    }
    public static void SetNearBallColr(int color)
    {
        NearBallExist = true; 
        NearBall = color; 

        // TODO :  do smth in UI
        Debug.Log("ball added to UI ..");
    }

    public static void RemoveNearBall()
    {
        NearBallExist = false;

        // TODO : do smth in UI
        Debug.Log("ball removed from UI .. ");
    }



    public static List<Drone> GetDrones()
    {
        return drones;
    }

    public static void AddDrone(Drone drone)
    {
        drones.Add(drone);
    }

    public static void AddPlayer(Player player)
    {
        players.Add(player);
    }

    public static List<Player> GetPlayers()
    {
        return players;
    }

    public static void SetWidthHight(int a, int b, float cellLengthP)
    {
        n = a;
        m = b;
        cellLength = cellLengthP;
        diagonalLength = (float)Math.Sqrt(2 * cellLengthP * cellLengthP);
    }

    public static void SetHouseGrid(Cell[,] grid)
    {
        houseGrid = grid;
    }

    public static Cell[,] GetHouseGrid()
    {
        return houseGrid;
    }

    public static List<KeyValuePair<int, int>>[,] GetAdjacList(int droneColor)
    {
        List<KeyValuePair<int, int>>[,] adj = new List<KeyValuePair<int, int>>[n, m];

        for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            adj[i, j] = new List<KeyValuePair<int, int>>();

        for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
        {
            if (i + 1 < n)
            {
                if (!houseGrid[i, j].HasUpWall() || houseGrid[i, j].GetUpColor() == droneColor)
                {
                    adj[i, j].Add(new KeyValuePair<int, int>(i + 1, j));
                    adj[i + 1, j].Add(new KeyValuePair<int, int>(i, j));
                }
            }

            if (j + 1 < m)
            {
                if (!houseGrid[i, j].HasRightWall() || houseGrid[i, j].GetRightColor() == droneColor)
                {
                    adj[i, j].Add(new KeyValuePair<int, int>(i, j + 1));
                    adj[i, j + 1].Add(new KeyValuePair<int, int>(i, j));
                }
            }
        }

        return adj;
    }

    public static int ReturnWidth()
    {
        return n;
    }

    public static int ReturnHeight()
    {
        return m;
    }

    public static float GetCellLength()
    {
        return cellLength; 
    }

    public static float GetDiagonalLength()
    {
        return diagonalLength;
    }



    public static void SetMinX(float value)
    {
        mnX = value; 
    }
    public static void SetMinY(float value)
    {
        mnY = value; 
    }

    public static float GetMinX()
    {
        return mnX; 
    }

    public static float GetMinY()
    {
        return mnY; 
    }
    public static void ClearPlayerAndDroneReferences()
    {
        // Clear or reset player and drone references
        players.Clear();
        drones.Clear();
    }



    public static int GetFurnitureSO_id(FurnitureSO f)
    {
        foreach(RoomTypeSO room in GetHouseRooms().roomTypes)
            foreach(FurnitureSO frn in room.roomObjects)
                if(frn == f)
                    return frn.id; 
        return -1; 
    }

    public static FurnitureSO GetFurnitureSO_byId(int i)
    {
        foreach(RoomTypeSO room in GetHouseRooms().roomTypes)
            foreach(FurnitureSO frn in room.roomObjects)
                if(frn.id == i)
                    return frn; 
        return null; 
    }
}