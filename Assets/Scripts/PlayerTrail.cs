using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PlayerTrail : MonoBehaviour
{
    // Start is called before the first frame update
    public float activeTime = 0.5f;
    [Header("Mesh related")]
    public float meshRefreshRate = 0.1f;
    public Transform postionToSpawn;
    public float meshDestroyDelay = 0.5f;

    public bool isTrailActive;
    private SkinnedMeshRenderer[] _skinnedMeshRenderers;


    

    // Update is called once per frame
    // void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.Q) && !isTrailActive)
    //     {
    //         StartCoroutine(ActiveTrail(activeTime));
    //     }
    // }

   public IEnumerator ActiveTrail(float timeActive)
    {
        while (timeActive > 0)
        {
            timeActive -= meshRefreshRate;
            if (_skinnedMeshRenderers == null)
            {
                _skinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
            }

            for (int i = 0; i < _skinnedMeshRenderers.Length; i++)
            {
                GameObject gObj = new GameObject();
                gObj.transform.SetPositionAndRotation(postionToSpawn.position, postionToSpawn.rotation);
                MeshRenderer mr = gObj.AddComponent<MeshRenderer>();
                MeshFilter mf = gObj.AddComponent<MeshFilter>();

                Mesh mesh = new Mesh();
                _skinnedMeshRenderers[i].BakeMesh(mesh);
                mr.materials = _skinnedMeshRenderers[i].materials;
                mf.mesh = mesh;
                Destroy(gObj,meshDestroyDelay);
            }

            yield return
                new WaitForSeconds(meshRefreshRate);
        }

        isTrailActive = false;
    }
}