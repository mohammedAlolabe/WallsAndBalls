using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private CinemachineVirtualCamera virtualCamera; // Reference to the Cinemachine VirtualCamera component
    private GameObject player; // Reference to the player GameObject
    private Transform target; // The target transform to follow
    private GameObject playerCameraRoot;
    private bool haveTarget = false; 

    private void Awake()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
    }

    private void Update() 
    {
        if(haveTarget) 
            return; 

        TryGetTarget(); 
    }


    private void TryGetTarget()
    {
        if(GlobalVariablesManager.GetPlayers().Count == 0) return; 

        player = GlobalVariablesManager.GetPlayers()[0].gameObject; 
        playerCameraRoot = player.transform.Find("PlayerCameraRoot").gameObject;
        
        if (playerCameraRoot != null)
        {
            target = playerCameraRoot.transform;

            if (virtualCamera != null)
            {
                virtualCamera.Follow = target;
            }
            else
            {
                Debug.LogWarning("CinemachineVirtualCamera component not found!");
            }
        }
        else
        {
            Debug.LogWarning("PlayerCameraRoot not found! Make sure to assign the appropriate tag to it.");
        }
        haveTarget = true; 
    }
}