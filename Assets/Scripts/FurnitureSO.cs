using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class FurnitureSO : ScriptableObject
{
    public int id;
    public Transform prefab; 
    public int widthCells, heightCells, maximumInRoom; 
    public bool wallBehindMust; 
    public bool cornerBehindMust; 
}
