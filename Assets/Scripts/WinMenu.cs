using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class WinMenu : MonoBehaviour
{
    [SerializeField] private GameObject restartWinButton;
    [SerializeField] private Button mainMenuWinButton;
    [SerializeField] private Button quitButton3;
    [SerializeField] private GameObject winMenuUI;
    [SerializeField] LevelManager levelManager;


    public void RestartGame()
    {
        Time.timeScale = 1f;
        GlobalVariablesManager.ClearPlayerAndDroneReferences();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Awake()
    {
        mainMenuWinButton.onClick.AddListener(() =>
        {
            GlobalVariablesManager.ClearPlayerAndDroneReferences();
            SceneManager.LoadScene(0);
        });

        quitButton3.onClick.AddListener(() =>
        {
            GlobalVariablesManager.ClearPlayerAndDroneReferences();
            Application.Quit();
        });
    }
    public void WinGame()
    {
        GlobalVariablesManager.EndGame();
        winMenuUI.SetActive(true);
        EventSystem.current.SetSelectedGameObject(restartWinButton);
        Time.timeScale = 0f;
        string WonLevel = File.ReadAllText(Application.persistentDataPath + "/level.txt");
        
        Debug.Log("****" + WonLevel);
        levelManager.StoreLevelInfo(WonLevel, OnLevelStored);
    }

    private void OnLevelStored(string message)
    {
        Debug.Log("Level Storage Message: " + message);
    }
}
