using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float timeLeft;
    private bool timerOn = false;
    [SerializeField] private TextMeshProUGUI timerTxt;

    private void Start()
    {
        timeLeft = 150; 
        timerOn = true;
        timerTxt.color = Color.white;
    }

    private void Update()
    {
        if(!timerOn) return; 


        bool somePlayerAttackedNow = false; 
        float decrease = Time.deltaTime;
        foreach(Player player in GlobalVariablesManager.GetPlayers())
            if(player.IsAttacked())
            {
                somePlayerAttackedNow = true; 
                decrease += Time.deltaTime * 2; 
            }

        GlobalVariablesManager.GetTakeDamage().SetIsDamage(somePlayerAttackedNow); 

        timeLeft = Mathf.Max(0, timeLeft - decrease); 
        updateTimer(timeLeft);  

        if(timeLeft == 0)
        {
            timerOn = false; 
            // TODO END GAME, LOSE
            GlobalVariablesManager.GetWinLosePause().GetComponent<LossMenu>().GameOver(); 
        }

        if (timeLeft < 60f)
        {
            timerTxt.color = Color.red;  
        }
    }

    private void updateTimer(float currentTime)
    {
        currentTime += 0.1f;
        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        timerTxt.text = string.Format("{0:00} : {1:00}", minutes, seconds);
    }
}
