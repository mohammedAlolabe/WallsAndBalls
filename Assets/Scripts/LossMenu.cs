using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LossMenu : MonoBehaviour
{
    [SerializeField] private GameObject restartLossButton;
    [SerializeField] private Button mainMenuLossButton;
    [SerializeField] private Button quitButton2;
    [SerializeField] private GameObject lossMenuUI;

    public void RestartGame() 
    {
        Time.timeScale = 1f;
        GlobalVariablesManager.ClearPlayerAndDroneReferences();

        // Unload the current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Awake()
    {
        mainMenuLossButton.onClick.AddListener(()=>{
            GlobalVariablesManager.ClearPlayerAndDroneReferences();
            SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
            SceneManager.LoadScene(0);
        });
        
        quitButton2.onClick.AddListener(()=>{
            GlobalVariablesManager.ClearPlayerAndDroneReferences();
            Application.Quit();
        });
    }

    public void GameOver()
    {
        GlobalVariablesManager.EndGame(); 
        EventSystem.current.SetSelectedGameObject(restartLossButton);
        lossMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }
}
