using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LevelManager : MonoBehaviour
{
    private const string baseUrl = "http://localhost:8000/api/";

    private void Start()
    {

    }

    public IEnumerator StoreLevel(string levelName, System.Action<string> callback)
    {
        string token = File.ReadAllText(Application.persistentDataPath + "/token.txt");
        string endpoint = "levels";
        string url = baseUrl + endpoint;

        WWWForm form = new WWWForm();
        form.AddField("name", levelName);

        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + token);


            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                string responseText = request.downloadHandler.text;
                BaseResponse response = JsonUtility.FromJson<BaseResponse>(responseText);
                Debug.Log("in Store Level Enumerator");
                callback?.Invoke(response.message);
            }
            else
            {
                callback?.Invoke("Error");
                Debug.LogError("Error: " + request.error);
            }
        }
    }

    // Simplified function to handle level storage
    public void StoreLevelInfo(string levelName, System.Action<string> callback)
    {
        Debug.Log("in Store Level Info");

        StartCoroutine(StoreLevel(levelName, callback));
    }

    public IEnumerator AssociateLevel(int level_id, string authToken, System.Action<string> callback)
    {
        string token = File.ReadAllText(Application.persistentDataPath + "/token.txt");
        string endpoint = "levels/associate";
        string url = baseUrl + endpoint;

        WWWForm form = new WWWForm();
        form.AddField("level_id", level_id);

        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + token);


            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                string responseText = request.downloadHandler.text;
                BaseResponse response = JsonUtility.FromJson<BaseResponse>(responseText);
                callback?.Invoke(response.message);
            }
            else
            {
                callback?.Invoke("Error");
                Debug.LogError("Error: " + request.error);
            }
        }
    }

    // Simplified function to handle level storage
    public void AssociateLevelInfo(int level_id, string authToken, System.Action<string> callback)
    {
        StartCoroutine(AssociateLevel(level_id, authToken, callback));
    }

    public IEnumerator GetPlayedLevels(int userId, System.Action<List<string>> callback)
    {
        string endpoint = "users/" + userId + "/levels";
        string url = baseUrl + endpoint;
        string token = File.ReadAllText(Application.persistentDataPath + "/token.txt");

        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            request.SetRequestHeader("Authorization", "Bearer " + token);
            request.SetRequestHeader("Accept", "application/json");

            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                string responseText = request.downloadHandler.text;
                LevelsResponse response = JsonUtility.FromJson<LevelsResponse>(responseText);

                List<string> levelNames = new List<string>();
                foreach (LevelInfo levelInfo in response.levels)
                {
                    levelNames.Add(levelInfo.name);
                }
                callback?.Invoke(levelNames);
            }
            else
            {
                callback?.Invoke(null);
                Debug.LogError("Error: " + request.error);
            }
        }


    }

    public void GetPlayedLevelNames(int userId, System.Action<List<string>> callback)
    {
        StartCoroutine(GetPlayedLevels(userId, callback));
    }

    public IEnumerator SearchUsers(string searchTerm, System.Action<List<UserInfo>> callback)
    {
        string endpoint = "users/search";
        string url = baseUrl + endpoint + "?username=" + System.Uri.EscapeDataString(searchTerm);
        string token = File.ReadAllText(Application.persistentDataPath + "/token.txt");

        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            request.SetRequestHeader("Authorization", "Bearer " + token);
            request.SetRequestHeader("Accept", "application/json");

            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                string responseText = request.downloadHandler.text;
                UsersResponse response = JsonUtility.FromJson<UsersResponse>(responseText);

                callback?.Invoke(response.users);
            }
            else
            {
                callback?.Invoke(null);
                Debug.LogError("Error: " + request.error);
            }
        }
    }

    // Simplified function to search users
    public void SearchUsersByUsername(string searchTerm, System.Action<List<UserInfo>> callback)
    {
        StartCoroutine(SearchUsers(searchTerm, callback));
    }
}



[System.Serializable]
public class UsersResponse
{
    public List<UserInfo> users;
}

[System.Serializable]
public class UserInfo
{
    public int id;
    public string username;
    public string created_at;
    public string updated_at;
}

[System.Serializable]
public class BaseResponse
{
    public string message;
}

[System.Serializable]
public class LevelsResponse
{
    public List<LevelInfo> levels;
}

[System.Serializable]
public class LevelInfo
{
    public int id;
    public string name;
    public string created_at;
    public string updated_at;
    public LevelPivot pivot;
}

[System.Serializable]
public class LevelPivot
{
    public int user_id;
    public int level_id;
    public string created_at;
    public string updated_at;
}


// How to use
// using UnityEngine;

// public class GameManager : MonoBehaviour
// {
//     [SerializeField] private LevelManager levelManager;

//     private void Start()
//     {
//         StoreLevel();
//     }

//     private void StoreLevel()
//     {
//         levelManager.StoreLevelInfo("LevelName", OnLevelStored);
//     }

//     private void OnLevelStored(string message)
//     {
//         Debug.Log("Level Storage Message: " + message);
//     }
// }
