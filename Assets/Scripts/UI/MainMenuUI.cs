using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private Button FriendsButton;

    private void Awake()
    {
        File.WriteAllText(Application.persistentDataPath + "/level.txt", "");

        playButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(1);
        });

        quitButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });

        FriendsButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(3);
        });
    }
}
