using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScrewDriver : MonoBehaviour
{
    public Image screwDriverEmpty;
    public Sprite screwDriverFull;

    private void Start() 
    {
        GlobalVariablesManager.SetScewDriver(this);     
    }
    
    public void SetScrewDriverOn()
    {
        screwDriverEmpty.sprite = screwDriverFull;
    }
}
