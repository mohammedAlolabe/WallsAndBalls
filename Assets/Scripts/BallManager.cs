using UnityEngine;


public class BallManager : MonoBehaviour
{
    [SerializeField] private GameObject coloredBall; // Reference to the player's colored ball in the right hand
    [SerializeField] private GameObject coloredBall2; // Reference to the player's colored ball in the left hand
    [SerializeField] private GameObject playerMaterials; // Reference to the player's materials object
    private bool canPickupColor = false;
    private GameObject player; // Reference to the player GameObject

    private bool toggleBall = false; // Flag to toggle between balls for color selection   
    

    public void PlayerTakesBall(Material material)
    {
        Material mat = material;
        Debug.Log("u here"+material);
        GameObject targetColoredBall = BallToggler.isRightBallActive ? coloredBall : coloredBall2;

            Renderer
                coloredBallRenderer =
                    targetColoredBall
                        .GetComponent<Renderer>(); // Get the Renderer component attached to the target colored ball

           
            // Check if the material is already present in the materials array of the colored ball
            bool materialExists = false;
            Material[] coloredBallMaterials = coloredBallRenderer.materials;
            for (int i = 0; i < coloredBallMaterials.Length; i++)
            {
                if (coloredBallMaterials[i] == mat)
                {
                    materialExists = true;
                    break;
                }
            }

            if (!materialExists)
            {
                // If there is space in the materials array, add the material
                if (coloredBallMaterials.Length < 1)
                {
                    Material[] newMaterials = new Material[coloredBallMaterials.Length + 1];

                    // Assign the triggered material to the new element
                    newMaterials[coloredBallMaterials.Length] = mat;

                    // Replace the materials array of the colored ball with the new array
                    coloredBallRenderer.materials = newMaterials;
                }
                else
                {
                    // Replace the first material in the array with the triggered material
                    coloredBallMaterials[0] = mat;
                    coloredBallRenderer.materials = coloredBallMaterials;
                }
                // Apply the color change to the player's materials
                SkinnedMeshRenderer playerSkinnedMeshRenderer = playerMaterials.GetComponent<SkinnedMeshRenderer>();
                Material[] playerMaterialsArray = playerSkinnedMeshRenderer.materials;
                for (int i = 0; i < playerMaterialsArray.Length; i++)
                {
                    playerMaterialsArray[i] = mat;
                }
                //playerMaterialsArray = groundBallMaterial;
                playerSkinnedMeshRenderer.materials = playerMaterialsArray;
                
            
        }

    
    }
}