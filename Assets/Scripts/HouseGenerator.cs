using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public static class HouseGenerator
{
    private static Cell[,] houseGrid;

    private static int n, m;
    private static int colorsNumber;

    private const int MIN_ROOM_LENGTH = 5, MAX_ROOM_LENGTH = 8;

    public static System.Random rnd;

    private static List<Room> rooms;

    private static HouseRenderer houseRenderer;

    private static int maximumDrones; 

    private static bool[, ] vis; 

    public static Cell[,] Generate(int width, int height, int colors, HouseRenderer houseRendererP, int maxDrones)
    {
        n = width;
        m = height;
        colorsNumber = colors;
        houseRenderer = houseRendererP;
        maximumDrones = maxDrones; 

        houseGrid = new Cell[n, m];
        vis = new bool[n, m];  
        rooms = new List<Room>();

        rnd = new System.Random();


        for(int i=0 ;i <n ;i ++)
            for(int j=0 ; j < m ;j ++)
                vis[i, j] = false; 

        // constructing the grid
        GridPartition();
        HandleUnvisited();
        RemoveSmallRooms();
        RemoveExtraWalls();

        // coloring 
        ColorWalls();


        // adding furniture & other stuff
        AddFurniture();
        AddDrones();
        AddBomb(); 
        AddTool(1);         AddTool(1); 
        AddTool(2);         AddTool(2); 
        AddPlayers(); 

        return houseGrid;
    }

    private static void AddDrones()
    {
        int addedDrones = 0;

        foreach (var room in rooms)
            if (addedDrones < maximumDrones && rnd.Next(0, 5) == 0) // probability to add a new drone is 1 / 5
            {
                foreach (var pos in room.myCells)
                {
                    if (houseGrid[pos.x, pos.y].HasFurniture() == false)
                    {
                        houseGrid[pos.x, pos.y].AddDrone(rnd.Next(0, houseRenderer.drones.Count));
                        addedDrones++;
                        break;
                    }
                }
            }

        Debug.Log("Drones in this level are : " + addedDrones + "  drones");
    }


    private static void GridPartition()
    {
        for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            houseGrid[i, j] = new Cell();

        Stack<RoomInitial> RoomInitials = new Stack<RoomInitial>();

        while ((RoomInitials.Count == 0 ||
                !(RoomInitials.Peek().UpperRight.x == m && RoomInitials.Peek().UpperRight.y == n)))
        {
            Position LowerLeft = null, UpperRight = null;

            if (RoomInitials.Count == 0) LowerLeft = new Position(0, 0);
            else if (RoomInitials.Peek().UpperRight.x == m)
                LowerLeft = new Position(0, RoomInitials.Peek().UpperRight.y);
            else LowerLeft = new Position(RoomInitials.Peek().UpperRight.x, RoomInitials.Peek().LowerLeft.y);

            UpperRight = new Position(
                rnd.Next(Math.Min(LowerLeft.x + MIN_ROOM_LENGTH, m), Math.Min(LowerLeft.x + MAX_ROOM_LENGTH, m + 1)),
                rnd.Next(Math.Min(LowerLeft.y + MIN_ROOM_LENGTH, n), Math.Min(LowerLeft.y + MAX_ROOM_LENGTH, n + 1))
            );

            if (m - UpperRight.x < MAX_ROOM_LENGTH) UpperRight.x = m;
            if (n - UpperRight.y < MAX_ROOM_LENGTH) UpperRight.y = n;

            RoomInitials.Push(new RoomInitial(LowerLeft, UpperRight));
        }


        foreach (RoomInitial RoomInitial in RoomInitials)
        {
            Room newRoom = new Room(rooms.Count + 1);

            for (int i = RoomInitial.LowerLeft.y; i < RoomInitial.UpperRight.y; i++)
            for (int j = RoomInitial.LowerLeft.x; j < RoomInitial.UpperRight.x; j++)
                if (houseGrid[i, j].roomId == -1)
                {
                    newRoom.AddPoint(new Position(i, j));
                    houseGrid[i, j].roomId = rooms.Count + 1;
                }

            rooms.Add(newRoom);
        }
    }

    private static void HandleUnvisited()
    {
        for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            if (houseGrid[i, j].roomId == -1)
            {
                Room newRoom = new Room(rnd.Next(rooms.Count + 1, 10000000));
                houseGrid[i, j].roomId = newRoom.id;

                newRoom.AddPoint(new Position(i, j));
                rooms.Add(newRoom);
            }
    }

    private static void RemoveSmallRooms()
    {
        int roomIndex;

        while (SmallRoomExist(out roomIndex) && rooms.Count > 1)
        {
            int mergeTo = GetAdjacentRoom(roomIndex);

            foreach (Position position in rooms[roomIndex].myCells)
            {
                rooms[mergeTo].AddPoint(position);
                houseGrid[position.x, position.y].roomId = rooms[mergeTo].id;
            }

            rooms.RemoveAt(roomIndex);
        }
    }

    private static bool SmallRoomExist(out int room)
    {
        room = -1;
        int mn = 1000000;
        for (int i = 0; i < rooms.Count; i++)
        {
            int minDimention = Math.Min(rooms[i].xs, rooms[i].ys);

            if (minDimention < mn && minDimention < MIN_ROOM_LENGTH)
            {
                mn = minDimention;
                room = i;
            }
        }

        return room != -1;
    }

    private static int GetAdjacentRoom(int roomInd)
    {
        foreach (Room room in rooms)
            if (room.id != rooms[roomInd].id)
                foreach (Position firstRoomPoint in room.myCells)
                foreach (Position secondRoomPoint in rooms[roomInd].myCells)
                {
                    int fr = Math.Abs(firstRoomPoint.x - secondRoomPoint.x);
                    int sc = Math.Abs(firstRoomPoint.y - secondRoomPoint.y);

                    if (Math.Min(fr, sc) == 0 && Math.Max(fr, sc) == 1)
                        return rooms.IndexOf(room);
                }

        return -1;
    }

    private static void RemoveExtraWalls()
    {
        for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
        {
            if (j < m - 1 && houseGrid[i, j].roomId == houseGrid[i, j + 1].roomId)
            {
                houseGrid[i, j].SetRightWall(false);
                houseGrid[i, j + 1].SetLeftWall(false);
            }

            if (i < n - 1 && houseGrid[i, j].roomId == houseGrid[i + 1, j].roomId)
            {
                houseGrid[i, j].SetUpWall(false);
                houseGrid[i + 1, j].SetDownWall(false);
            }
        }
    }


    private static void ColorWalls()
    {
        int paintColor = 0;

        // right side walls : 
        for (int j = 0; j < (m - 1); j++)
        for (int i = 0; i < n; i++)
            if (houseGrid[i, j].HasRightWall())
            {
                if (houseGrid[i, j].HasDownWall() || houseGrid[i, j].HasUpWall() || (j + 1 < m &&
                        (houseGrid[i, j + 1].HasUpWall() || houseGrid[i, j + 1].HasDownWall())))
                    paintColor = HouseGenerator.rnd.Next(1, HouseGenerator.colorsNumber);
                else
                {
                    houseGrid[i, j].SetRightColor(paintColor);
                    if (j + 1 < m) houseGrid[i, j + 1].SetLeftColor(paintColor);
                }
            }

        // up side walls : 
        for (int i = 0; i < (n - 1); i++)
        for (int j = 0; j < m; j++)
            if (houseGrid[i, j].HasUpWall())
            {
                if (houseGrid[i, j].HasRightWall() || houseGrid[i, j].HasLeftWall() || (i + 1 < n &&
                        (houseGrid[i + 1, j].HasRightWall() || houseGrid[i + 1, j].HasLeftWall())))
                    paintColor = HouseGenerator.rnd.Next(1, HouseGenerator.colorsNumber);
                else
                {
                    houseGrid[i, j].SetUpColor(paintColor);
                    if (i + 1 < n) houseGrid[i + 1, j].SetDownColor(paintColor);
                }
            }
    }


    private static void AddBomb()
    {
        for(int k =0 ; k < 1000000 ; k ++)
        {
            int i = rnd.Next(2, n - 2), j = rnd.Next(2, m - 2); 

            bool can = true; 

            for(int dx = -1 ; dx <= +1 ; dx ++)
                for(int dy = -1 ; dy <= +1 ; dy ++)
                    can &= !vis[i + dx, j + dy] && houseGrid[i + dx, j + dy].roomId == houseGrid[i, j].roomId; 

            if(can)
            {
                houseGrid[i, j].SetBomp(); 
                return;
            }
        }

        Debug.LogError("Bomb haven't been generated !");
    }


    private static void AddTool(int tool)
    {
        for(int k =0 ; k < 1000000 ; k ++)
        {
            int i = rnd.Next(0, n), j = rnd.Next(0, m); 

            if(houseGrid[i, j].HasFurniture() && !houseGrid[i, j].HasBall() && !houseGrid[i, j].HasTool())
            {
                houseGrid[i, j].SetTool(tool); 
                Debug.Log("tool " + tool + "was generated in cell : " + i + " " + j);
                return; 
            }
        }

        Debug.LogError("Bomb haven't been generated !");
    }


    private static void AddFurniture()
    {
        // generate furniture for each room 
        foreach (Room room in rooms)
        {
            RoomTypeSO roomTypeSO =
                houseRenderer.houseRoomsSO.roomTypes[rnd.Next(0, houseRenderer.houseRoomsSO.roomTypes.Count)];
            int objectsCount = roomTypeSO.roomObjects.Count;

            // stores cells postions depending how many walls surrounding them
            // objects wich should be generated
            // 0 -> corner object 
            // 1 -> wall object
            // 2 -> anywhere object 
            List<Position>[] cellBasedOnType = new List<Position>[3];
            for (int i = 0; i < 3; i++) cellBasedOnType[i] = new List<Position>();
            foreach (Position position in room.myCells)
                if (!houseGrid[position.x, position.y].HasNonWhiteWalls())
                {
                    if (houseGrid[position.x, position.y].NumberOfOnWalls() > 1) cellBasedOnType[0].Add(position);
                    else if (houseGrid[position.x, position.y].NumberOfOnWalls() == 1) cellBasedOnType[1].Add(position);
                    else cellBasedOnType[2].Add(position);
                }


            List<int>[] objectsToGenerate = new List<int>[3];
            for (int i = 0; i < 3; i++) objectsToGenerate[i] = new List<int>();

            for (int i = 0; i < objectsCount; i++)
            {
                int repeat = roomTypeSO.roomObjects[i].maximumInRoom;
                int inList = 2;
                if (roomTypeSO.roomObjects[i].cornerBehindMust) inList = 0;
                else if (roomTypeSO.roomObjects[i].wallBehindMust) inList = 1;

                for (int j = 0; j < repeat; j++)
                    objectsToGenerate[inList].Add(i);
            }


            for (int i = 0; i < 3; i++)
            {
                int counter = 0;
                while (counter < 50 && objectsToGenerate[i].Count > 0 && cellBasedOnType[i].Count > 0)
                {
                    int randomObjectToGenerate = rnd.Next(0, objectsToGenerate[i].Count);
                    FurnitureSO generateFurniture =
                        roomTypeSO.roomObjects[objectsToGenerate[i][randomObjectToGenerate]];

                    int randomCell = rnd.Next(0, cellBasedOnType[i].Count);
                    Position cellPosition = cellBasedOnType[i][randomCell];

                    // check if we can area fits generated object area  
                    bool can = true;
                    for (int ii = -generateFurniture.heightCells + 1; ii < generateFurniture.heightCells; ii++)
                    for (int jj = -generateFurniture.widthCells + 1; jj < generateFurniture.widthCells; jj++)
                    {
                        bool found = false;
                        foreach (Position pos in cellBasedOnType[i])
                            found |= (pos.x == cellPosition.x + ii) && (pos.y == cellPosition.y + jj);
                        can &= found;
                    }

                    if (can)
                    {
                        houseGrid[cellPosition.x, cellPosition.y].SetFurnitureSO(generateFurniture);

                        objectsToGenerate[i].RemoveAt(randomObjectToGenerate);
                        cellBasedOnType[i].RemoveAt(randomCell);

                        for (int ii = 0; ii < generateFurniture.widthCells; ii++)
                        for (int jj = 0; jj < generateFurniture.heightCells; jj++)
                        {
                            int newI = cellPosition.x + ii, newJ = cellPosition.y + jj; 
                            cellBasedOnType[i].Remove(new Position(newI, newJ));

                            if(newI < n && newJ < m)
                                vis[newI, newJ] = true; 
                        }
                    }

                    counter++;
                }
            }
        }
    }

    private static void AddPlayers(int players = 1)
    {
        for(int k=0 ; k < players ; k ++)
        { 
            bool generated = false; 
            int mx = 50000; 
            for(int r=0 ; r < mx ; r ++)
            {
                int i = rnd.Next(0, n), j = rnd.Next(0, m);
                if(!houseGrid[i, j].HasFurniture()) 
                {
                    houseGrid[i, j].SetHasPlayer(); 
                    Debug.Log("Player position is : " + i + " " + j); 
                    generated = true; 
                    break;
                }
            }

            if(!generated)
                Debug.LogError("player hasn't been generated !!!"); 
        }
    }
}


public class Position
{
    public int x, y;

    public Position(int i, int j)
    {
        x = i;
        y = j;
    }
}

public class RoomInitial
{
    public Position LowerLeft, UpperRight;

    public RoomInitial(Position first, Position second)
    {
        LowerLeft = first;
        UpperRight = second;
    }
}

public class Room
{
    public List<Position> myCells;
    public List<int> x, y;

    public int id;

    public int xs, ys;

    public Room(int roomId)
    {
        myCells = new List<Position>();

        x = new List<int>();
        y = new List<int>();

        id = roomId;
        xs = ys = 0;
    }

    public void AddPoint(Position position)
    {
        myCells.Add(position);

        if (!x.Contains(position.x))
        {
            x.Add(position.x);
            xs++;
        }

        if (!y.Contains(position.y))
        {
            y.Add(position.y);
            ys++;
        }
    }
}