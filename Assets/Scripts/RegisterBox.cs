using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RegisterBox : MonoBehaviour
{
    public Button submitButton;
    public TMP_InputField username;
    public TMP_InputField firstName;
    public TMP_InputField lastName;
    public TMP_InputField password;

    private string usernameValue;
    private string firstNameValue;
    private string lastNameValue;
    private string passwordValue;
    private void Start()
    {
        if (System.IO.File.Exists(Application.persistentDataPath + "/token.txt"))
        {
            // The file exists -> run event
            SceneManager.LoadScene(0);
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            submitButton.onClick.Invoke();
            Debug.Log("Button Pressed!");
        }
    }

    [SerializeField] private RegistrationManager registrationManager;
    public void Register()
    {
        usernameValue = username.text;
        firstNameValue = firstName.text;
        lastNameValue = lastName.text;
        passwordValue = password.text;
        // StartCoroutine(registrationManager.RegisterUser(usernameValue, passwordValue));
        registrationManager.RegisterUserAndGetInfo(usernameValue, passwordValue, OnRegistrationComplete);

    }


    private void OnRegistrationComplete(string message, string token)
    {
        if (token != null)
        {
            Debug.Log("Registration Message: " + message);
            Debug.Log("Registration Token: " + token);
            SceneManager.LoadScene(0);
        }
    }
    // private void OnRegistrationComplete(string message, string token)
    // {
    //     Debug.Log("Registration Message: " + message);
    //     Debug.Log("Registration Token: " + token);
    // }
}
