using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureObject : MonoBehaviour
{
    private bool hasBall = false; 
    private int ballColor; 

    private bool hasTool = false; 
    private int toolType; // 1 or 2 

    public bool HasTool()
    {
        return hasTool; 
    }

    public void RemoveTool()
    {
        hasTool = false;
    }

    public void SetTool(int type)
    {
        hasTool = true; 
        toolType = type;
    }

    public int GetToolType()
    {
        return toolType; 
    }

    public void SetBall(int color)
    {
        ballColor = color; 
        hasBall = true; 
    }

    public void RemoveBall()
    {
        if(!hasBall) return; 

        hasBall = false; 
        GlobalVariablesManager.RemoveNearBall(); 
    }

    public bool HasBall()
    {
        return hasBall; 
    }

    public int GetBallColor()
    {
        return ballColor; 
    }
    
}
