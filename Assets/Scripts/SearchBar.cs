using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UIElements;

public class SearchBar : MonoBehaviour
{
   
   [SerializeField] LevelManager levelManager;
   public TMP_InputField usernameSearch;

   private string usernameSValue;

   private void  Awake()
   {
      
   }

   public void StoreSearch()
   {
      LevelsMemory.levels = new List<string>();
      usernameSValue = usernameSearch.text;
      levelManager.SearchUsersByUsername(usernameSValue, onSearchComplete);
      Debug.Log("Stored values: " + usernameSValue);
   }

   private void onSearchComplete(List<UserInfo> users)
   {
      Debug.Log("fuck bisher");
      Debug.Log("number of users shown in search: " + users.Count);
      foreach (var i in users)
      {
         Debug.Log("search found user: " + i.id);
      }
      if(users.Count > 0){
         Debug.Log("search found user with i: " + users[0].id);
         levelManager.GetPlayedLevelNames(users[0].id, onGetPlayedLevelsComplete);
      }
   }

   private void onGetPlayedLevelsComplete(List<string> levels){
      LevelsMemory.levels = levels;
      foreach(var l in levels){
         Debug.Log("onGetPlayedLevelsComplete level found: " +l);
      }
   }
}
