using Microsoft.Unity.VisualStudio.Editor;
using UnityEngine;

public class BallToggler : MonoBehaviour
{
    public GameObject rightBall;
    public GameObject leftBall;
    public GameObject playerMaterials; // Reference to the player's materials object
    public static bool isRightBallActive = true;
    private Ball_1_Selected ball1Selected;
    private Ball_2 ball2;
    private RectTransform ball1RectTransform;
    private RectTransform ball2RectTransform;
    private Vector3 baseScale1; // Store the base scale of Ball 1's UI image
    private Vector3 baseScale2; // Store the base scale of Ball 1's UI image

    private void Start()
    {
        // Activate the right ball and deactivate the left ball at the start
        rightBall.SetActive(true);
        leftBall.SetActive(false);
        // Find the player materials object
        playerMaterials = GameObject.Find("Player_Mats");
        // Get references to the Ball_1_Selected and Ball_2 scripts
        // Use Find to get the Image components of ball1 and ball2
        ball1Selected = GameObject.Find("Canvas/Bar/Ball1").GetComponent<Ball_1_Selected>();
        ball2 = GameObject.Find("Canvas/Bar/Ball2").GetComponent<Ball_2>();
        ball1RectTransform = GameObject.Find("Canvas/Bar/Ball1").GetComponent<RectTransform>();
        ball2RectTransform = GameObject.Find("Canvas/Bar/Ball2").GetComponent<RectTransform>();
        
        // Store the base scale of Ball 1's UI image
        baseScale1 = ball1RectTransform.localScale;
        baseScale2 = ball2RectTransform.localScale;

        // Set the initial colors of the UI images based on the right and left balls
        ball1Selected.image.color = rightBall.GetComponent<Renderer>().material.color;
        ball2.image.color = leftBall.GetComponent<Renderer>().material.color;
        ball1RectTransform.localScale = Vector3.one; 
        ball2RectTransform.localScale = Vector3.one; 
        // Set the right hand as active at start
        isRightBallActive = true;
        ball1RectTransform.localScale = new Vector3(baseScale1.x*1.5f, baseScale1.y * 1.5f, baseScale1.z); // Increase the size in the +y axis (height)
        
        
    }

    private void Update()
    {
        // Check for input to toggle the balls
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ToggleBalls();
            TogglePlayerMaterials();

        }
        // Update the colors of the UI images based on the right and left balls
        ball1Selected.image.color = rightBall.GetComponent<Renderer>().material.color;
        ball2.image.color = leftBall.GetComponent<Renderer>().material.color;
    }

    private void ToggleBalls()
    {
        // Toggle the active state of the balls
        isRightBallActive = !isRightBallActive;
        rightBall.SetActive(isRightBallActive);
        leftBall.SetActive(!isRightBallActive);

        // Print which ball is currently active
        if (isRightBallActive)
        {
            Debug.Log("Right ball is active.");
            ball1RectTransform.localScale = new Vector3(baseScale1.x*1.5f, baseScale1.y * 1.5f, baseScale1.z); // Increase the size in the +y axis (height)
            ball2RectTransform.localScale = new Vector3(baseScale2.x, baseScale2.y, baseScale2.z); // Increase the size in the +y axis (height)
        }
        else
        {
            Debug.Log("Left ball is active.");
            ball2RectTransform.localScale = new Vector3(baseScale2.x*1.5f, baseScale2.y * 1.5f, baseScale2.z); // Increase the size in the +y axis (height)
            ball1RectTransform.localScale = new Vector3(baseScale1.x, baseScale1.y , baseScale1.z); // Increase the size in the +y axis (height)
        }
      
    }
    private void TogglePlayerMaterials()
    {
        // Get the SkinnedMeshRenderer component of the player's materials object
        SkinnedMeshRenderer playerSkinnedMeshRenderer = playerMaterials.GetComponent<SkinnedMeshRenderer>();
        
        // Get the materials of the player's materials object
        Material[] playerMaterialsArray = playerSkinnedMeshRenderer.materials;
        
        // Toggle the active state of the materials based on the current ball
        for (int i = 0; i < playerMaterialsArray.Length; i++)
        {
            // Set the material based on the active ball
            if (isRightBallActive)
            {
                playerMaterialsArray[i] = rightBall.GetComponent<Renderer>().material;
            }
            else
            {
                playerMaterialsArray[i] = leftBall.GetComponent<Renderer>().material;
            }
        }
        
        // Update the materials of the player's materials object
        playerSkinnedMeshRenderer.materials = playerMaterialsArray;
    }
}