using UnityEngine;
using UnityEngine.UI;

public class LevelsManager : MonoBehaviour
{
    public Button buttonPrefab;
    public Transform content;
    public float buttonSpacing = 10f;
    public int buttonsPerRow = 3; // Number of buttons in each row

    private Vector2 buttonSize;
    private int numberOfRows = 0;
    private int numberOfButtons = 0;

    private void Start()
    {
        buttonSize = buttonPrefab.GetComponent<RectTransform>().sizeDelta;
    }

    public void GenerateButtonOnClick()
    {
        float xOffset = (numberOfButtons % buttonsPerRow) * (buttonSize.x + buttonSpacing);
        float yOffset = -numberOfRows * (buttonSize.y + buttonSpacing);

        Button newButton = Instantiate(buttonPrefab, content);
        RectTransform buttonTransform = newButton.GetComponent<RectTransform>();
        buttonTransform.anchoredPosition = new Vector2(xOffset, yOffset);

        int buttonIndex = numberOfButtons; // Prevent closure issues
        newButton.onClick.AddListener(() => OnButtonClicked(buttonIndex));

        numberOfButtons++;

        if (numberOfButtons % buttonsPerRow == 0)
        {
            numberOfRows++;
        }

        UpdateContentSize();
    }

    void UpdateContentSize()
    {
        float contentHeight = numberOfRows * (buttonSize.y + buttonSpacing);
        content.GetComponent<RectTransform>().sizeDelta = new Vector2(content.GetComponent<RectTransform>().sizeDelta.x, contentHeight);
    }

    void OnButtonClicked(int buttonIndex)
    {
        Debug.Log("Button " + buttonIndex + " clicked!");
    }
}