using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.IO;
public class RegistrationManager : MonoBehaviour
{
    private const string baseUrl = "http://localhost:8000/api/";


    private void Start()
    {
    }

    public IEnumerator RegisterUser(string username, string password, System.Action<string, string> callback)
    {
        string endpoint = "signup";
        string url = baseUrl + endpoint;

        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            request.SetRequestHeader("Accept", "application/json");

            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                string responseText = request.downloadHandler.text;
                RegistrationResponse response = JsonUtility.FromJson<RegistrationResponse>(responseText);
                callback?.Invoke(response.message, response.token);
                Debug.Log("Token: " + response.token);
                File.WriteAllText(Application.persistentDataPath + "/token.txt", response.token);
            }
            else
            {
                callback?.Invoke("Error", null);
                Debug.LogError("Error: " + request.error);
            }
        }

    }
    public void RegisterUserAndGetInfo(string username, string password, System.Action<string, string> callback)
    {
        StartCoroutine(RegisterUser(username, password, callback));
    }
}



[System.Serializable]
public class RegistrationResponse
{
    public string message;
    public string token;
}


// EXAMPLE USAGE FOR REGISTRATION
// using UnityEngine;

// public class GameManager : MonoBehaviour
// {
//     [SerializeField] private RegistrationManager registrationManager;

//     private void Start()
//     {
//         registrationManager.RegisterUserAndGetInfo("yourUsername", "yourPassword", OnRegistrationComplete);
//     }

//     private void OnRegistrationComplete(string message, string token)
//     {
//         Debug.Log("Registration Message: " + message);
//         Debug.Log("Registration Token: " + token);
//     }
// }

