using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private LayerMask furnitureMask;

    public event EventHandler<OnSelectedFurnitureObjectChangedArgs> OnSelectedFurnitureObjectChanged;

    public class OnSelectedFurnitureObjectChangedArgs : EventArgs
    {
        public FurnitureObject SelectedFurnitureP;
    }

    private FurnitureObject selectedFurniture = null;

    private bool attacked = false;
    [SerializeField] private BallManager ballManager; // Reference to the BallManager script
    [SerializeField] private GameObject bm;


    private void Start()
    {
        Transform bm = GameObject.Find("PlayerArmature").transform;

        ballManager = bm.GetComponent<BallManager>();
    }

    private void Awake()
    {
        GlobalVariablesManager.AddPlayer(this);
    }


    private void Update()
    {
        HandleSelectedFurniture();
        HandleBomb();
        GetObjectFromSelectedFurniture();
        HandleIsAttacked();
        HandleSelectedFurnitureContnetVisualising(); 
    }

    public bool IsAttacked()
    {
        return attacked;
    }

    private void HandleIsAttacked()
    {
        attacked = false;
        foreach (Drone drone in GlobalVariablesManager.GetDrones())
        {
            double x = drone.transform.position.x, y = drone.transform.position.z;

            double dx = Math.Abs(x - transform.position.x),
                dy = Math.Abs(y - transform.position.z);

            float dist = (float)Math.Sqrt(dx * dx + dy * dy);

            attacked |= dist < drone.GetDamageDistance() && drone.NearPlayer();
        }
    }

    private void SetSelectedFurnitureObject(FurnitureObject furnitureObject)
    {
        if (furnitureObject == selectedFurniture) return;
        this.selectedFurniture = furnitureObject;

        OnSelectedFurnitureObjectChanged?.Invoke(this, new OnSelectedFurnitureObjectChangedArgs
        {
            SelectedFurnitureP = this.selectedFurniture,
        });
    }

    private void HandleSelectedFurniture()
    {
        Debug.DrawRay(transform.position + new Vector3(0, 2, 0),
            transform.TransformDirection(Vector3.forward) + new Vector3(0, -1f, 0), Color.green, 11, true);

        if (Physics.Raycast(transform.position + new Vector3(0, 2, 0),
                transform.TransformDirection(Vector3.forward) + new Vector3(0, -1f, 0), out RaycastHit raycastHit,
                1000000, furnitureMask))
        {
            if (raycastHit.transform.TryGetComponent<FurnitureObject>(out FurnitureObject obj))
            {
                SetSelectedFurnitureObject(obj);
            }
            else
            {
                SetSelectedFurnitureObject(null);
            }
        }
        else
        {
            SetSelectedFurnitureObject(null);
        }
    }


    private void HandleBomb()
    {
        const float interactDistance = 5;
        if (Physics.Raycast(transform.position + new Vector3(0, 2, 0),
                transform.TransformDirection(Vector3.forward) + new Vector3(0, -1f, 0), out RaycastHit raycastHit,
                interactDistance))
        {
            if (raycastHit.transform.TryGetComponent<Bomb>(out Bomb obj) &&
                GlobalVariablesManager.HaveAllTools()){
                GlobalVariablesManager.GetWinLosePause().GetComponent<WinMenu>().WinGame();}
        }
    }

    private void GetObjectFromSelectedFurniture()
    {
        if (selectedFurniture != null && Input.GetKeyDown(KeyCode.R))
        {
            if (selectedFurniture.HasTool())
            {
                GlobalVariablesManager.SetTool(selectedFurniture.GetToolType());
                selectedFurniture.RemoveTool();
            }

            if (selectedFurniture.HasBall())
            {
                int materialIndex = selectedFurniture.GetBallColor() - 1;
                Material sendThisMaterialToFunction = GlobalVariablesManager.GetBallsMaterials()[materialIndex];
                ballManager.PlayerTakesBall(sendThisMaterialToFunction);
                selectedFurniture.RemoveBall();
            }
        }
    }
    
    private void HandleSelectedFurnitureContnetVisualising()
    {
        if(selectedFurniture == null || (!selectedFurniture.HasBall() && !selectedFurniture.HasTool()))
        {
            GlobalVariablesManager.GetContent().Hide(); 
            return; 
        }
        else 
        {
            GlobalVariablesManager.GetContent().Show(); 

            if(selectedFurniture.HasTool())
                GlobalVariablesManager.GetContent().AddTool(selectedFurniture.GetToolType()); 
            
            if(selectedFurniture.HasBall())
                GlobalVariablesManager.GetContent().AddBall(selectedFurniture.GetBallColor()); 
        }
    }
}