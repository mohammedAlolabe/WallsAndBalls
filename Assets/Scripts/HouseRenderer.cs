using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class HouseRenderer : MonoBehaviour
{
    [SerializeField][Range(20, 50)] private int width = 100, height = 100;

    [SerializeField] private float scale = 2;

    [SerializeField] private Transform floorPrefab;

    [SerializeField] private List<Transform> walls;
    [SerializeField] private List<Material> ballsMaterials;
    [SerializeField] public HouseRoomsSO houseRoomsSO;

    [SerializeField] public List<GameObject> drones;

    [SerializeField] private int maximumDrones = 2;

    [SerializeField] private GameObject bomb;
    [SerializeField] private GameObject playerPrefab;

    private const float EXTRA_FOR_WALLS = 1.5f;


    private void Start()
    {
        foreach (Material material in ballsMaterials)
            GlobalVariablesManager.AddMaterial(material);
        GlobalVariablesManager.SetHouseRoomSO(houseRoomsSO);
        string importedLevel = File.ReadAllText(Application.persistentDataPath + "/level.txt");
        if (importedLevel.Length == 0)
        {
            Debug.Log("new level poop");
            StartNewLevel();
        }
        else
        {
            StartImportedLevel(importedLevel);
            Debug.Log("imported level poop");
        }
    }

    public void StartNewLevel()
    {
        LevelData level = new LevelData(width, height, HouseGenerator.Generate(width, height, walls.Count, this, maximumDrones));
        Debug.Log(level.LevelDataToString());
        string levelString = level.LevelDataToString();
        Debug.Log(levelString);
        level = new LevelData(levelString);
        File.WriteAllText(Application.persistentDataPath + "/level.txt", levelString);


        Draw(level.n, level.m, level.grid);
    }

    public void StartImportedLevel(string importedLevel)
    {
        LevelData level = new LevelData(importedLevel);
        Draw(level.n, level.m, level.grid);
    }

    private void Draw(int width, int height, Cell[,] grid)
    {
        GlobalVariablesManager.SetWidthHight(width, height, scale);
        GlobalVariablesManager.SetHouseGrid(grid);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Cell cell = grid[i, j];
                var position = new Vector3((float)-1 * height / 2 + j, 0, (float)-1 * width / 2 + i) * scale;

                if (cell.HasBomb())
                {
                    var b = Instantiate(bomb, transform);
                    b.transform.position = position;
                }

                if (cell.HasPlayer())
                {
                    var pl = Instantiate(playerPrefab, transform);
                    pl.transform.position = position;
                }

                if (cell.HasFurniture())
                {
                    var furn = Instantiate(cell.GetFurnitureSO().prefab, transform) as Transform;
                    furn.position = position;

                    if (cell.HasBall())
                        furn.GetComponent<FurnitureObject>().SetBall(cell.GetBallColor());

                    if (cell.HasTool())
                        furn.GetComponent<FurnitureObject>().SetTool(cell.GetToolType());


                    float degree = 0;
                    if (cell.GetFurnitureSO().wallBehindMust)
                    {
                        switch (cell.GetOnWall())
                        {
                            case 0:
                                degree = 0;
                                break;

                            case 1:
                                degree = 180;
                                break;

                            case 2:
                                degree = 90;
                                break;

                            case 3:
                                degree = -90;
                                break;
                        }
                    }
                    else if (cell.GetFurnitureSO().cornerBehindMust)
                    {
                        degree = cell.GetOnCorner() * 90;
                    }

                    furn.localEulerAngles = new Vector3(0, degree, 0);
                }


                var floor = Instantiate(floorPrefab, transform) as Transform;
                floor.position = position;
                floor.localScale *= scale;
                floor.eulerAngles = new Vector3(90, 0, 0);


                if (cell.HasRightWall())
                {
                    var rightWall = Instantiate(walls[cell.GetRightColor()], transform) as Transform;
                    rightWall.position = position + new Vector3(0.5f, 0, 0) * scale +
                                         new Vector3(0, (scale + EXTRA_FOR_WALLS) / 2, 0);
                    rightWall.eulerAngles = new Vector3(0, 90, 0);
                    rightWall.localScale *= scale;
                    rightWall.localScale = rightWall.localScale + new Vector3(0, EXTRA_FOR_WALLS, 0);
                }

                if (cell.HasUpWall())
                {
                    var topWall = Instantiate(walls[cell.GetUpColor()], transform) as Transform;
                    topWall.position = position + new Vector3(0, 0, 0.5f) * scale +
                                       new Vector3(0, (scale + EXTRA_FOR_WALLS) / 2, 0);
                    topWall.localScale *= scale;
                    topWall.localScale = topWall.localScale + new Vector3(0, EXTRA_FOR_WALLS, 0);
                }

                if (cell.HasLeftWall() && (j == 0 || !grid[i, j - 1].HasRightWall()))
                {
                    var leftWall = Instantiate(walls[cell.GetLeftColor()], transform) as Transform;
                    leftWall.position = position + new Vector3(-0.5f, 0, 0) * scale +
                                        new Vector3(0, (scale + EXTRA_FOR_WALLS) / 2, 0);
                    leftWall.eulerAngles = new Vector3(0, 90, 0);
                    leftWall.localScale *= scale;
                    leftWall.localScale = leftWall.localScale + new Vector3(0, EXTRA_FOR_WALLS, 0);


                    if (i == 0 && j == 0)
                        GlobalVariablesManager.SetMinX(leftWall.transform.position.x);
                }

                if (cell.HasDownWall() && (i == 0 || !grid[i - 1, j].HasUpWall()))
                {
                    var bottomWall = Instantiate(walls[cell.GetDownColor()], transform) as Transform;
                    bottomWall.position = position + new Vector3(0, 0, -0.5f) * scale +
                                          new Vector3(0, (scale + EXTRA_FOR_WALLS) / 2, 0);
                    bottomWall.localScale *= scale;
                    bottomWall.localScale = bottomWall.localScale + new Vector3(0, EXTRA_FOR_WALLS, 0);

                    if (i == 0 && j == 0)
                        GlobalVariablesManager.SetMinY(bottomWall.transform.position.z);
                }

                if (cell.HasDrone())
                {
                    var drone = Instantiate(drones[cell.GetDroneColor()], transform);
                    drone.transform.position = position + Vector3.up * 3;
                    drone.GetComponent<Drone>().ActivateDrone();
                    GlobalVariablesManager.AddDrone(drone.GetComponent<Drone>());
                }
            }
        }
    }
}