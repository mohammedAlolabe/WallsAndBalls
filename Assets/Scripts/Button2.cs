using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Button2 : MonoBehaviour
{
    [SerializeField] private Button B2;
    private int id = 2;
    
    public void OnClickB2() 
    {
        //GlobalVariablesManager.ClearPlayerAndDroneReferences();
         //GlobalVariablesManager.ClearPlayerAndDroneReferences();
        if(LevelsMemory.levels.Count >= id){
            System.IO.File.WriteAllText(Application.persistentDataPath + "/level.txt", LevelsMemory.levels[id-1]);
            SceneManager.LoadScene(1);
        }
        // Unload the current scene
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
