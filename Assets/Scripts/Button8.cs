using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Button8 : MonoBehaviour
{
    [SerializeField] private Button B8;
    private int id = 8;

    public void OnClickB8()
    {
        //GlobalVariablesManager.ClearPlayerAndDroneReferences();
        if (LevelsMemory.levels.Count >= id)
        {
            System.IO.File.WriteAllText(Application.persistentDataPath + "/level.txt", LevelsMemory.levels[id - 1]);
            SceneManager.LoadScene(1);
        }
        // Unload the current scene
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
