using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selected : MonoBehaviour
{
    [SerializeField]
    private FurnitureObject furnitureObject;
    [SerializeField] private GameObject[] visualGameObjects;

    bool activated = false; 
    private void Start()
    {
        foreach (var player in GlobalVariablesManager.GetPlayers())
            player.OnSelectedFurnitureObjectChanged += Player_OnSelectedFurnitureObjectChanged;
    }

    private void Player_OnSelectedFurnitureObjectChanged(object sender, Player.OnSelectedFurnitureObjectChangedArgs e)
    {
        if (e.SelectedFurnitureP != null && e.SelectedFurnitureP == furnitureObject)
        {
            Show(); 
        }
        else 
        {
            Hide(); 
        }
    }

    private void Show()
    {
        foreach (var gameObject in visualGameObjects)
            gameObject.SetActive(true);

        if(furnitureObject.HasBall())
            GlobalVariablesManager.SetNearBallColr(furnitureObject.GetBallColor()); 

        activated = true; 
    }

    private void Hide()
    {
        foreach (GameObject gameObject in visualGameObjects)
            gameObject.SetActive(false);

        if(activated)
            GlobalVariablesManager.RemoveNearBall();
    
        activated = false; 
    }
}
