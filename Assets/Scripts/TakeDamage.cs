using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class TakeDamage : MonoBehaviour
{
    private bool damageOn = false; 
    private float intensity = 0;
    PostProcessVolume Volume;
    Vignette Vignette;
    // Start is called before the first frame update
    void Start()
    {
        GlobalVariablesManager.SetTakeDamage(this); 
        Volume = GetComponent<PostProcessVolume>();
        Volume.profile.TryGetSettings<Vignette>(out Vignette);

        if (!Vignette)
        {
            print("Error, vignette empty");
        }
        else
        {
            Vignette.enabled.Override(false);
        }
    }

    private void Update() {
        if(damageOn) StartCoroutine(StartEffect());
        else StartCoroutine(StopEffect());
    }

    public void SetIsDamage(bool dmg)
    {
        damageOn = dmg; 
    }

    private IEnumerator StartEffect()
    {
        intensity = 0.4f;
        // give the effect to start appear
        Vignette.enabled.Override(true);

        //give the intensity the value 0.4 that make the effect visual appear
        Vignette.intensity.Override(0.4f);
        yield break;
    }
    private IEnumerator StopEffect()
    {
        while (intensity > 0)
        {
            // here we are decrease the intensity smoother 
            // we can change this value and control it
            intensity -= 0.01f;

            //make sure that intnesity number don't go less than 0
            if (intensity < 0) intensity = 0;

            //let the intensity get the new changable value  
            Vignette.intensity.Override(intensity);

            //give small and smooth precision from the max values that we have which is 0.4
            yield return new WaitForSeconds(0.1f);
        }
                // stop the effect
        Vignette.enabled.Override(false);
    }

    private IEnumerator TakeDamageEffect()
    {
        //the value of the intensity that I want to start the effect with
        intensity = 0.4f;

        // give the effect to start appear
        Vignette.enabled.Override(true);

        //give the intensity the value 0.4 that make the effect visual appear
        Vignette.intensity.Override(0.4f);

        // wait 0.4 s that is the duration of the effect
        yield return new WaitForSeconds(0.4f);

        // after making the effect we are going to remove it slowly
        while (intensity > 0)
        {
            // here we are decrease the intensity smoother 
            // we can change this value and control it
            intensity -= 0.01f;

            //make sure that intnesity number don't go less than 0
            if (intensity < 0) intensity = 0;

            //let the intensity get the new changable value  
            Vignette.intensity.Override(intensity);

            //give small and smooth precision from the max values that we have which is 0.4
            yield return new WaitForSeconds(0.1f);
        }
        // stop the effect
        Vignette.enabled.Override(false);

        // exiting from the function
        yield break;
    }
}
