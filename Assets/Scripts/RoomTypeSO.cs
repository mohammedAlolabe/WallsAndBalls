using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class RoomTypeSO : ScriptableObject
{
    public string roomTypeName; 
    public List<FurnitureSO> roomObjects;
    public int priority;
    public int minimumNumberOfDronesInRoom = 0, maximumNumberOfDronesInRoom = 2; 
}
