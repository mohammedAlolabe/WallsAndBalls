using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Skillcd : MonoBehaviour
{
    [SerializeField] private Image imageCooldown;

    [SerializeField] private TMP_Text textCooldown;
    public bool isInsideCollider = false;
    // variables for cd
    private bool isCooldown = false;
    public float cooldownTime =10.0f;
    private float cooldownTimer = 0.0f;
    void Start()
    {
        textCooldown.gameObject.SetActive(false);
        imageCooldown.fillAmount = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetKeyDown(KeyCode.Q))
        // {
        //     UseSkill();
        // }
        if (isCooldown)
        {
            ApplyCoolDown();
        }
    }

    void ApplyCoolDown()
    {
        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer < 0.0f)
        {
            isCooldown = false;
            textCooldown.gameObject.SetActive(false);
            imageCooldown.fillAmount = 0.0f;
        }
        else
        {
            textCooldown.text = Mathf.RoundToInt(cooldownTimer).ToString();
            imageCooldown.fillAmount = cooldownTimer / cooldownTime;
            // Log the remaining cooldown time
            //Debug.Log("Remaining cooldown time: " + Mathf.RoundToInt(cooldownTimer));
        }
    }

    public void UseSkill()
    {
        if (isCooldown)
        {
            // user has clicked skill while in use 
        }
        else if (isInsideCollider)
        {
            // Skill is off cooldown and player is inside the collider, use the skill
            isCooldown = true;
            textCooldown.gameObject.SetActive(true);
            cooldownTimer = cooldownTime;
            
            Debug.Log("Using the skill!");
        }
        else
        {
            Debug.Log("Cannot use the skill outside the collider!");
            
        }
    }
    public bool CanUseSkill()
    {
        return !isCooldown;
    }
    public float GetCooldownTimer()
    {
        return cooldownTimer;
    }
}
