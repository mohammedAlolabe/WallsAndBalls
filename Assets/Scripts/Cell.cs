
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell 
{
    private bool[] walls;
    private int[] colors;
    public int roomId;
    private bool hasDrone;
    private int droneColor;

    private bool hasFurniture;
    private FurnitureSO furnitureSO;

    private bool hasBall; 
    private int ballColor; 

    private bool hasBomb; 

    private bool hasTool = false; 
    private int toolType; 

    private bool hasPlayer; 

    public bool HasPlayer()
    {
        return hasPlayer; 
    }

    public void SetHasPlayer()
    {
        hasPlayer = true; 
    }


    public void SetTool(int type)
    {
        hasTool = true; 
        toolType = type; 
    }

    public bool HasTool()
    {
        return hasTool; 
    }

    public int GetToolType()
    {
        return toolType; 
    }

    public void AddDrone(int color)
    {
        hasDrone = true;
        droneColor = color; 
    }

    public bool HasDrone()
    {
        return hasDrone;
    }

    public int GetDroneColor()
    {
        return droneColor; 
    }

    public bool HasBall()
    {
        return hasBall;
    }

    public int GetBallColor()
    {
        return ballColor;
    }

    public void SetBomp()
    {
        hasBomb = true; 
    }

    public bool HasBomb()
    {
        return hasBomb; 
    }

    public  Cell()
    {        
        walls = new bool[]{true, true, true, true}; 
        colors = new int[4];

        hasPlayer = false; 
        hasBomb = false; 
        furnitureSO = null;
        hasFurniture = false;
        hasDrone = false;
        hasBall = false; 
        hasTool = false; 


        for (int i = 0; i < 4; i++)
            colors[i] = 0; // all faces are initially white 

        roomId = -1; // not defined yet 
    }

    public Cell(int[] values)
    {
        walls = new bool[]{true, true, true, true}; 
        colors = new int[4];

        hasPlayer = false; 
        hasBomb = false; 
        furnitureSO = null;
        hasFurniture = false;
        hasDrone = false;
        hasBall = false; 
        hasTool = false; 


        for (int i = 0; i < 4; i++)
            colors[i] = 0; // all faces are initially white 

        roomId = -1; // not defined yet 
        



        Debug.Log("New cell data : !");
        for(int i=0 ;i < 19 ; i ++)
            Debug.Log(values[i]);

        for(int i=0 ; i < 4 ; i ++)
            walls[i] = (values[i] == 1) ? true : false; 
        
        for(int i=0 ;i < 4 ; i ++)
            colors[i] = values[4 + i]; 

        roomId = values[8]; 

        hasDrone = (values[9] == 1) ? true : false; 
        droneColor = values[10]; 

        hasBall = (values[11] == 1) ? true : false; 
        ballColor = values[12]; 

        hasFurniture = (values[13] == 1) ? true : false; 
        if(hasFurniture)
            furnitureSO = GlobalVariablesManager.GetFurnitureSO_byId(values[14]); 

        hasBomb = (values[15] == 1) ? true : false; 

        hasTool = (values[16] == 1) ? true : false; 
        toolType = values[17]; 

        hasPlayer = (values[18] == 1) ? true : false; 
    }

    public string StringifyCell()
    {
        int[] values = new int[19]; 

        values[0] = (walls[0] == true) ? 1 : 0;
        values[1] = (walls[1] == true) ? 1 : 0;
        values[2] = (walls[2] == true) ? 1 : 0;
        values[3] = (walls[3] == true) ? 1 : 0;

        values[4] = colors[0];
        values[5] = colors[1];
        values[6] = colors[2];
        values[7] = colors[3];

        values[8] = roomId;

        values[9] = (hasDrone == true) ? 1 : 0;
        values[10] = droneColor;

        values[11] = (hasBall == true) ? 1 : 0;
        values[12] = ballColor;

        values[13] = (hasFurniture == true) ? 1 : 0;
        if(hasFurniture)
            values[14] = GlobalVariablesManager.GetFurnitureSO_id(furnitureSO); 
        
        values[15] = (hasBomb == true) ? 1 : 0; 

        values[16] = (hasTool == true) ? 1 : 0; 
        values[17] = toolType;  

        values[18] = (hasPlayer == true) ? 1 : 0; 

        return string.Join(",", values);
    }

    
    public int GetOnCorner()
    {
        if (walls[0] && walls[2]) return 0;
        if (walls[2] && walls[1]) return 1;
        if (walls[1] && walls[3]) return 2;
        if (walls[3] && walls[0]) return 3;

        return -1;
    }

    public int GetOnWall()
    {
        for (int i = 0; i < 4; i++)
            if (walls[i] == true)
                return i;

        return -1;
    }

    public FurnitureSO GetFurnitureSO()
    {
        return furnitureSO;
    }

    public void SetFurnitureSO(FurnitureSO furnitureSOP)
    {
        hasFurniture = true;
        furnitureSO = furnitureSOP;

        const int COLORS_CNT = 5;
        int randomNumber = HouseGenerator.rnd.Next(0, 30); 
        if(randomNumber > 0 && randomNumber <= COLORS_CNT)
        {
            // probabiltiy for object to have a ball is 1 / 6 
            hasBall = true; 
            ballColor = randomNumber; 
        }
    }

    public bool HasFurniture()
    {
        return hasFurniture;
    }

    public int NumberOfOnWalls()
    {
        int cnt = 0;
        for (int i = 0; i < 4; i++)
            if (walls[i] == true)
                cnt++;
        return cnt;
    }

    public bool HasNonWhiteWalls()
    {
        for (int i = 0; i < 4; i++)
            if (colors[i] != 0)
                return true;

        return false;
    }

    public bool HasUpWall()
    {
        return walls[0];
    }

    public bool HasDownWall()
    {
        return walls[1];
    }

    public bool HasRightWall()
    {
        return walls[2];
    }

    public bool HasLeftWall()
    {
        return walls[3];
    }

    public void SetUpWall(bool status)
    {
        walls[0] = status;
    }

    public void SetDownWall(bool status)
    {
        walls[1] = status;
    }

    public void SetRightWall(bool status)
    {
        walls[2] = status;
    }

    public void SetLeftWall(bool status)
    {
        walls[3] = status;
    }

    public int GetUpColor()
    {
        return colors[0];
    }

    public int GetDownColor()
    {
        return colors[1];
    }

    public int GetRightColor()
    {
        return colors[2];
    }

    public int GetLeftColor()
    {
        return colors[3];
    }

    public void SetUpColor(int color)
    {
        walls[0] = true;
        colors[0] = color;
    }

    public void SetDownColor(int color)
    {
        walls[1] = true;
        colors[1] = color;
    }

    public void SetRightColor(int color)
    {
        walls[2] = true;
        colors[2] = color;
    }

    public void SetLeftColor(int color)
    {
        walls[3] = true;
        colors[3] = color;
    }
}



