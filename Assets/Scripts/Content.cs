using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Content : MonoBehaviour
{
    public Image original;
    public Sprite screwDriver;
    public Sprite wrench;
    public Sprite red;
    public Sprite yellow;
    public Sprite orange;
    public Sprite blue;
    public Sprite cyan;
    public Sprite purple;
    [SerializeField] private GameObject maParent; 
    
    public void Show()
    {
        maParent.SetActive(true); 
    }

    public void Hide()
    {
        maParent.SetActive(false); 
    }

    private void Start() 
    {
        GlobalVariablesManager.SetContent(this); 
        Hide(); 
    }


    public void AddBall(int i)
    {
        Debug.Log("*** " + i);
        switch(i)
        {
            case 1 : 
                original.sprite = blue; 
                break; 
            case 2 : 
                original.sprite = cyan; 
                break; 
            case 3 : 
                original.sprite = orange; 
                break; 
            case 4 : 
                original.sprite = purple;
                break; 
            case 5 : 
                original.sprite = red; 
                break;
        }
        Debug.Log(original.sprite);
        NotEmpty(); 
    }

    public void AddTool(int i)
    {
        original.sprite = (i == 1) ? wrench : screwDriver;
        NotEmpty(); 
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            original.sprite = screwDriver;
            NotEmpty();
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            original.sprite = wrench;
            NotEmpty();
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            original.sprite = red;
            NotEmpty();
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            original.sprite = blue;
            NotEmpty();
        }
    }
    private void NotEmpty()
    {
        original = GetComponent<Image>();
        var tempColor = original.color;
        tempColor.a = 1f;
        original.color = tempColor;
    }
}
